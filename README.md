# homebrew-update

Automatically update repos and upgrade all formulae and casks, then circumvent MacOS's stupid application quarantine feature

## Installation

Clone repo

    mkdir ~/git
    cd ~/git
    git clone git@git.galactic.cat:wolre/homebrew-update.git

Copy files to correct place

    cd homebrew-update
    mkdir /Users/$(whoami)/bin
    cp -r * /Users/$(whoami)/bin/

## Uninstall

    rm /Users/$(whoami)/bin/hbu
    rm -r /Users/$(whoami)/bin/homebrew_update
    rm /Users/$(whoami)/git/homebrew-update

