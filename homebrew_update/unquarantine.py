import subprocess
import os

path = "/Applications/"


def get_files(folder_path):
    file_names = []
    # Check if the folder exists
    if os.path.exists(folder_path):
        #print(os.listdir(folder_path))
        # Iterate through all files in the folder
        return os.listdir(folder_path)
    else:
        print("Folder does not exist!")
    return file_names


items = get_files(path)

for i in items:
    if i[len(i)-4:] == '.app':
        print("Unquarantining " + path + i)
        subprocess.run("sudo xattr -dr com.apple.quarantine \"" + path + i + "\"", shell = True)
    else:
        print("Skipping " + path + i)

exit()
